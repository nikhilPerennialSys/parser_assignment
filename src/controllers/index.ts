import { NextFunction, Request, Response } from "express";
import Invoice from "../models/invoices";
import {
  getDataByCurrency,
  getDataByDate,
  getDataByStatus,
  xmlParseFun,
} from "../services";

export const fillData = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    if (!req.file) {
      throw new Error("File Not Found");
    }
    const fileMimeType: string = req.file?.mimetype.split("/")[1].toLowerCase();
    // console.log(fileMimeType.split("/")[1]);
    const data = await xmlParseFun(req.file?.path, fileMimeType, next);
    console.log("RETURNED DATA");
    console.log(data);
    if (data?.success) {
      const result = await Invoice.bulkCreate(data.data);
      res.json({
        success: true,
        result,
      });
    } else {
      res.status(400).json({
        success: false,
        errors: data?.data,
      });
    }
  } catch (error) {
    next(error);
  }
};

export const getByCurrency = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const currency: string = req.params.currency;
    console.log(currency);
    if (!currency) {
      throw new Error("Provide Currency To Search");
    }
    const data = await getDataByCurrency(currency, next);
    if (data) {
      res.json({
        success: true,
        total_records: data.length,
        data,
      });
    }
  } catch (error) {
    next(error);
  }
};

export const getByStatus = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const status: string = req.params.status;
    console.log(status);
    if (!status) {
      throw new Error("Provide status To Search");
    }
    const data = await getDataByStatus(status, next);
    if (data) {
      res.json({
        success: true,
        total_records: data.length,
        data,
      });
    }
  } catch (error) {
    next(error);
  }
};

export const getByDates = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const date1: string = req.params.date1;
    const date2: string = req.params.date2;
    if (!date1 || !date2) {
      throw new Error("Please Provide 2 Dates");
    }
    const data = await getDataByDate(date1, date2, next);
    if (data) {
      res.json({
        success: true,
        total_records: data.length,
        data,
      });
    }
  } catch (error) {
    next(error);
  }
};
