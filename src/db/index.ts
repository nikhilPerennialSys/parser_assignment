import { Sequelize } from "sequelize";
import db from "../config/index";

const sequelize = new Sequelize(db.DB, db.USER, db.PASSWORD, {
  host: db.HOST,
  dialect: "mysql",
  pool: {
    min: db.pool.min,
    max: db.pool.max,
    acquire: db.pool.acquire,
    idle: db.pool.idle,
  },
});

export default sequelize;

