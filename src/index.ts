import express, { NextFunction, Request, Response } from "express";
import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import sequelizeConnection from "./db/index";
import Invoice_Model from "./models/invoices";
import invoice_routes from "./routes/index";

dotenv.config();

const app = express();

const port = process.env.PORT || 8000;

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));

sequelizeConnection.sync().then((res) => {
  console.log("Synced");
});

Invoice_Model.sync()
  .then((res) => {
    console.log("Table Synced");
  })
  .catch((err) => {
    console.log(err);
  });

app.use("/invoice", invoice_routes);

app.use("*", (req: Request, res: Response) => {
  return res.status(404).json({
    success: false,
    message: "API Endpoint Doesn't Exist",
  });
});

app.use(function (
  err: { message: string; statuscode: number; code: string },
  req: Request,
  res: Response,
  next: NextFunction
) {
  console.log("ERROR", err.message);
  if (err.code === "LIMIT_FILE_SIZE") {
    res.status(400).json({
      success: false,
      message: "File Size Limit is 1 MB",
    });
  }
  if (err.message && err.statuscode) {
    res.status(err.statuscode).json({ success: false, message: err.message });
  }
  if (err.message) {
    res.status(400).json({ success: false, message: err.message });
  } else {
    res.status(500).json({ success: false, message: "Internal Server Error" });
  }
});

app.listen(port, () => {
  console.log(`Server Started on http://localhost:${port}`);
});
