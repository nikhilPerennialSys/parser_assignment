import { DataTypes, DateDataType, Model, Optional, Sequelize } from "sequelize";
import sequelizeConnection from "../db";

export interface InvoiceAttr {
  invoiceID: string;
  // amount: number;
  payment: string;
  transaction_date: DateDataType;
  status: string;
}

export interface UserInput<InvoiceAttr> {}

// export interface IngredientOuput extends Required<InvoiceAttr> {}

class Invoice_Model extends Model<InvoiceAttr> implements InvoiceAttr {
  public invoiceID!: string;
  // public amount!: number;
  public payment!: string;
  public transaction_date!: DateDataType;
  public status!: string;
}

Invoice_Model.init(
  {
    invoiceID: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    payment: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    transaction_date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize: sequelizeConnection,
  }
);

export default Invoice_Model;
