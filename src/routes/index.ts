import { Router } from "express";
import multer from "multer";
import { v4 } from "uuid";
import {
  fillData,
  getByCurrency,
  getByDates,
  getByStatus,
} from "../controllers";

const router = Router();

const diskStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    const mimetype = file.mimetype;
    const mimetype_splitted = mimetype.split("/");
    if (
      mimetype_splitted.includes("csv") ||
      mimetype_splitted.includes("xml")
    ) {
      const uuid = v4() + file.originalname;
      const name = uuid;
      file.originalname = name;
      cb(null, "./uploads");
    } else {
      cb(new Error("Expected File CSV / XML Type"), "");
    }
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const uploadStrategy = multer({
  storage: diskStorage,
  limits: {
    fileSize: 1024 * 1024,
  },
}).single("data");

router.post("/uploadFile", uploadStrategy, fillData);

router.get("/getByCurrency/:currency", getByCurrency);

router.get("/getByStatus/:status", getByStatus);

router.get("/getByDate/:date1/:date2", getByDates);

export default router;
