import { NextFunction } from "express";
import xmlParse from "xml2js";
import fs from "fs";
import validateCurrencyCode from "validate-currency-code";
import Invoice_Model, { InvoiceAttr } from "../models/invoices";
import csvTojson from "csvtojson";
import { Op } from "sequelize";

export const xmlParseFun = async (
  file: any,
  fileMimeType: string,
  next: NextFunction
) => {
  try {
    let allRes: InvoiceAttr[] | any[] = [];
    let errors: { Number_of_node: number; message: string }[] = [];
    let cnt: number = 0;
    switch (fileMimeType) {
      case "xml":
        xmlParse.parseString(
          fs.readFileSync(file).toString(),
          async (err, result) => {
            if (err) {
              errors.push({
                Number_of_node: cnt,
                message: err.message,
              });
            } else {
              allRes = result.Transactions.Transaction.map((item: any) => {
                cnt++;
                if (!item["$"]) {
                  errors.push({
                    Number_of_node: cnt,
                    message: "Inovice ID Missing",
                  });
                }
                if (
                  !item.PaymentDetails[0].Amount[0] ||
                  !item.PaymentDetails[0].CurrencyCode[0]
                ) {
                  errors.push({
                    Number_of_node: cnt,
                    message: "Payment Details Missing",
                  });
                }
                if (
                  item.PaymentDetails[0].CurrencyCode[0] &&
                  !validateCurrencyCode(item.PaymentDetails[0].CurrencyCode[0])
                ) {
                  errors.push({
                    Number_of_node: cnt,
                    message: "Invalid Currency Code",
                  });
                }
                if (!item.TransactionDate[0]) {
                  errors.push({
                    Number_of_node: cnt,
                    message: "Transaction Date Missing",
                  });
                }
                if (!item.Status[0]) {
                  errors.push({
                    Number_of_node: cnt,
                    message: "Status Missing",
                  });
                }
                // console.log(item.Status[0].toLowerCase());
                else {
                  let status = item.Status[0].toLowerCase();
                  if (
                    status !== "approved" &&
                    status !== "rejected" &&
                    status !== "done"
                  ) {
                    errors.push({
                      Number_of_node: cnt,
                      message: "Invalid Status Code",
                    });
                  }
                }
                if (errors.length < 1) {
                  return {
                    invoiceID: item["$"].id,
                    payment:
                      item.PaymentDetails[0].Amount[0] +
                      " " +
                      item.PaymentDetails[0].CurrencyCode[0],
                    // amount:item.PaymentDetails[0].Amount[0],
                    // currency_code:item.PaymentDetails[0].CurrencyCode[0],
                    transaction_date: item.TransactionDate[0],
                    status:
                      item.Status[0] === "Approved"
                        ? "A"
                        : item.Status[0] === "Rejected"
                        ? "R"
                        : "D",
                  };
                }
              });
            }
          }
        );
        if (errors.length > 0) {
          fs.unlinkSync(file);
          return {
            success: false,
            data: errors as any[],
          };
        } else {
          fs.unlinkSync(file);
          return {
            success: true,
            data: allRes as InvoiceAttr[],
          };
        }
        break;

      case "csv":
        // papaParser.parse(fs.createReadStream(file), {
        //   header: true,
        //   complete: async (result) => {
        //     // console.log(result);
        //     allRes = result.data.map((item: any) => {
        //       cnt++;
        //       if (!item["invoiceID"]) {
        //         errors.push({
        //           Number_of_node: cnt,
        //           message: "Inovice ID Missing",
        //         });
        //         return;
        //       }
        //       if (!item["payment"]) {
        //         errors.push({
        //           Number_of_node: cnt,
        //           message: "Payment Details Missing",
        //         });
        //         return;
        //       }
        //       if (!item["transaction_date"]) {
        //         errors.push({
        //           Number_of_node: cnt,
        //           message: "Transaction Date Missing",
        //         });
        //         return;
        //       }
        //       if (!item["status"]) {
        //         errors.push({
        //           Number_of_node: cnt,
        //           message: "Transaction Date Missing",
        //         });
        //         return;
        //       } else {
        //         // console.log("In ELSE");
        //         if (errors.length < 1) {
        //           return item;
        //         }
        //       }
        //     });
        //     // console.log(allRes);
        //     await fs.unlinkSync(file);
        //     const data = {
        //       success: true,
        //       data: allRes as InvoiceAttr[],
        //     };
        //     console.log(data);
        //     if (allRes.length > 0) {
        //       return data;
        //     }
        //   },
        // });
        const arr = await csvTojson().fromFile(file);
        allRes = arr.map(
          (item: {
            invoiceID: string;
            payment: string;
            transaction_date: string;
            status: string;
          }) => {
            cnt++;
            if (!item.invoiceID) {
              errors.push({
                Number_of_node: cnt,
                message: "InoviceID Required",
              });
              return;
            }
            if (!item.status) {
              errors.push({
                Number_of_node: cnt,
                message: "Status Required",
              });
              return;
            } else if (
              item.status.toLowerCase() !== "approved" &&
              item.status.toLowerCase() !== "done" &&
              item.status.toLowerCase() !== "rejected"
            ) {
              errors.push({
                Number_of_node: cnt,
                message: "Invalid Status",
              });
              return;
            }
            if (!item.transaction_date) {
              errors.push({
                Number_of_node: cnt,
                message: "Transaction Date Required",
              });
              return;
            }
            if (!item.payment) {
              errors.push({
                Number_of_node: cnt,
                message: "Payment Details Required",
              });
              return;
            } else if (!validateCurrencyCode(item.payment.split(" ")[1])) {
              errors.push({
                Number_of_node: cnt,
                message: "Invalid Currency Code",
              });
              return;
            } else {
              if (errors.length < 1) {
                return {
                  invoiceID: item.invoiceID,
                  payment: item.payment,
                  transaction_date: item.transaction_date,
                  status:
                    item.status === "Approved"
                      ? "A"
                      : item.status === "Rejected"
                      ? "R"
                      : "D",
                };
              }
            }
          }
        );
        break;
    }
    if (errors.length > 0) {
      fs.unlinkSync(file);
      return {
        success: false,
        data: errors as any[],
      };
    } else {
      fs.unlinkSync(file);
      return {
        success: true,
        data: allRes as InvoiceAttr[],
      };
    }
  } catch (error) {
    next(error);
  }
};

export const getDataByCurrency = async (
  currency: string,
  next: NextFunction
) => {
  try {
    if (!validateCurrencyCode(currency)) {
      throw new Error("Invalid Currency Code");
    }
    const data = await Invoice_Model.findAll({
      where: {
        payment: {
          [Op.like]: `% ${currency}`,
        },
      },
    });
    if (data.length < 1) {
      throw new Error("No Data Found");
    } else return data;
  } catch (error) {
    next(error);
  }
};

export const getDataByStatus = async (
  statusReceived: string,
  next: NextFunction
) => {
  try {
    if (
      !statusReceived ||
      (statusReceived.toLowerCase() !== "done" &&
        statusReceived.toLowerCase() !== "approved" &&
        statusReceived.toLowerCase() !== "rejected")
    ) {
      throw new Error("Invalid Status");
    } else {
      if (statusReceived.toLowerCase() === "approved") {
        statusReceived = "A";
      } else if (statusReceived.toLowerCase() === "done") {
        statusReceived = "D";
      } else {
        statusReceived = "R";
      }
    }
    const data = await Invoice_Model.findAll({
      where: { status: statusReceived },
    });
    if (data.length < 1) {
      throw new Error("No Data Found");
    } else return data;
  } catch (error) {
    next(error);
  }
};

export const getDataByDate = async (
  date1: string,
  date2: string,
  next: NextFunction
) => {
  try {
    if (
      !date1.match(
        "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]"
      ) ||
      !date2.match(
        "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]"
      )
    ) {
      throw new Error("Invalid Date Format (Required yyyy-mm-dd hh:mm:ss)");
    }
    const data = await Invoice_Model.findAll({
      where: {
        transaction_date: {
          [Op.between]: [new Date(date1), new Date(date2)],
        },
      },
    });
    if (data.length < 1) {
      throw new Error("No Data Found");
    } else return data;
  } catch (error) {
    next(error);
  }
};
